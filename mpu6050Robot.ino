#include<Servo.h>
#include<Wire.h>
const int MPU=0x69;   // default = 0x68
int AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ;
void get6050();
Servo servox;
Servo servoy;
int xpos;
int ypos;
void setup()
{
  Wire.begin();
  Wire.beginTransmission(MPU);
  Wire.write(0x6B);
  Wire.write(0);
  Wire.endTransmission(true);
  Serial.begin(9600);
  servoy.attach(10);
  servox.attach(11);
  servoy.write(70);
  servox.write(70);
}
void loop()
{
  get6050();
  xpos=map(AcX,-16383,16383,0,180);
  ypos=map(AcY,-16383,16383,0,180);
  servoy.write(xpos);
  servox.write(ypos);
  Serial.print("xpos=");Serial.print(xpos);Serial.print("/ypos=");Serial.println(ypos);
  delay(15);
 
}
void get6050(){
  Wire.beginTransmission(MPU);
  Wire.write(0x3B);
  Wire.endTransmission(false);
  Wire.requestFrom(MPU,14,true);
  AcX=Wire.read()<<8|Wire.read();
  AcY=Wire.read()<<8|Wire.read();
  AcZ=Wire.read()<<8|Wire.read();
  Tmp=Wire.read()<<8|Wire.read();
  GyX=Wire.read()<<8|Wire.read();
  GyY=Wire.read()<<8|Wire.read();
  GyZ=Wire.read()<<8|Wire.read();
}